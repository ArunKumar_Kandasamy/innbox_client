require 'rubygems'
require 'active_resource'  
class Resource < ActiveResource::Base
end


class GalleryImage < Resource
     
      self.format = ::JsonFormatter.new(:gallery_images)
      self.site = 'http://www.innboxapi.in/api/v1'
     #self.site = 'http://localhost:3000/api/v1'
      attr_accessor :current_user_token
      attr_writer :deferred_image
      belongs_to :gallery_type

     def self.current_user_token(token)
         Resource.headers['authorization'] = "Token " + "token=#{token}"
     end

end