require 'rubygems'
require 'active_resource'  
class Resource < ActiveResource::Base
end


class GalleryType < Resource
     
     self.format = ::JsonFormatter.new(:gallery_types)
     self.site = 'http://www.innboxapi.in/api/v1'
     #self.site = 'http://localhost:3000/api/v1'
     
     attr_accessor :current_user_token
     

     def self.current_user_token(token)
         Resource.headers['authorization'] = "Token " + "token=#{token}"
     end

     def gallery_images(scope = :all)
        GalleryImage.find(scope, :params => {:gallery_type_id => self.id})
      end

     def gallery_image(id)
        gallery_images(id)
      end

end