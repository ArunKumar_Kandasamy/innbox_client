require 'rubygems'
require 'active_resource'  
class Resource < ActiveResource::Base
end

class Tag < Resource
      self.format = ::JsonFormatter.new(:tags)
      self.site = 'http://www.innboxapi.in/api/v1'
     #self.site = 'http://localhost:3000/api/v1'
end
