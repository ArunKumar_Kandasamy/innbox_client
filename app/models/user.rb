require 'rubygems'
require 'active_resource'  
class Resource < ActiveResource::Base
   class << self

    def headers
      Thread.current["active.resource.currentthread.headers"] = {} if Thread.current["active.resource.currentthread.headers"].blank?
      Thread.current["active.resource.currentthread.headers"]
    end

  end
end


class User < Resource
     
     self.format = ::JsonFormatter.new(:users)
     self.site = 'http://www.innboxapi.in/api/v1'
     #self.site = 'http://localhost:3000/api/v1'

     
     attr_accessor :current_user_token, :password, :password_confirmation

     def self.current_user_token(token)
             Resource.headers['authorization'] = "Token " + "token=#{token}"
     end

end