class StaticPagesController < ApplicationController

  def index
  end

  def contact
  end

  def aboutus
  end

  def privacy
  end

  def policy
  end

  def investors_desk
  end

  def documentation
  end

  def catch_404
    raise ActionController::RoutingError.new(params[:path])
  end

end