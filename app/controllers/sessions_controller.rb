class SessionsController < ApplicationController
    
      def new
            if  current_user
                redirect_to dashboard_path
            else 
                render 'new'
            end
      end

      def create 
            @session = Session.create(user_params)
            if @session.attributes[:authentication_token].present?
                 session[:user_token] = @session.attributes[:authentication_token]
                 session[:user_id] = @session.attributes[:id]
                 session[:email] = @session.attributes[:email]
                 flash[:notice] = " Logged in successfully "
                 redirect_to dashboard_path
            else
                 flash[:warning] = " Invalid email or password "
                 render 'new'
            end
      end

      def destroy
           session[:user_token] = nil
           session[:user_id] = nil
            session[:email] = nil
            flash[:error] = " Logged out! "
            redirect_to login_url
      end

      private

            def user_params
                params.require(:session).permit( :email, :password)
            end

end