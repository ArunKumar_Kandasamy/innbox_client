class AccountActivationsController < ApplicationController
   def edit
    user = AccountActivation.create(:email => params[:email],:id => params[:id])
    if user 
      flash[:notice] = "Account activated!"
      redirect_to login_url
    else
      flash[:notice] = "Invalid activation link"
      redirect_to login_url
    end
  end
end
