class SettingsController < ApplicationController
      before_action :pass_token
      before_action :set_setting, only: [:show, :edit, :update, :destroy]

      # GET /settings
      # GET /settings.json
      def index
        @settings = Setting.all
      end

      # GET /settings/1
      # GET /settings/1.json
      def show
          @settings = Setting.all
          render 'index'
      end

      # GET /settings/new
      def new
        if Setting.first.present?
            @settings = Setting.all
             render 'index'
        else
          @setting = Setting.new
        end
      end

      # GET /settings/1/edit
      def edit
          @settings = Setting.all
          render 'index'
      end

      # POST /settings
      # POST /settings.json
      def create
            @setting = Setting.new(setting_params)
            respond_to do |format|
                if @setting.save
                  format.html { redirect_to @setting, notice: 'Setting was successfully created.' }
                  format.json { render :show, status: :created, location: @setting }
                else
                  format.html { render :new }
                  format.json { render json: @setting.errors, status: :unprocessable_entity }
                end
            end
      end

      # PATCH/PUT /settings/1
      # PATCH/PUT /settings/1.json
      def update
        respond_to do |format|
          @setting.verify_user = !@setting.verify_user
          if  @setting.save
            format.html { redirect_to settings_path, notice: 'Setting was successfully updated.' }
            format.json { render :show, status: :ok, location: @setting }
          else
            format.html { render :edit }
            format.json { render json: @setting.errors, status: :unprocessable_entity }
          end
        end
      end

      # DELETE /settings/1
      # DELETE /settings/1.json
      def destroy
        @setting.destroy
        respond_to do |format|
          format.html { redirect_to settings_url, notice: 'Setting was successfully destroyed.' }
          format.json { head :no_content }
        end
      end

      private
            def pass_token
                  @user_id = session[:user_id]
                  if @user_id.class != NilClass
                      Setting.current_user_token(session[:user_token])
                  else
                    redirect_to login_url
                  end
             end

            # Use callbacks to share common setup or constraints between actions.
            def set_setting
                @setting = Setting.find(params[:id])
            end

            # Never trust parameters from the scary internet, only allow the white list through.
            def setting_params
              params.require(:setting).permit(:verify_user)
            end
end
