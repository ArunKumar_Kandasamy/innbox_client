class CmsPagesController < ApplicationController
      before_action :pass_token
      before_action :set_cms_page, only: [:show, :edit, :update, :destroy]
      

      # GET /cms_pages
      # GET /cms_pages.json
      def index
        @cms_pages = CmsPage.all
      end

      # GET /cms_pages/1
      # GET /cms_pages/1.json
      def show
      end

      # GET /cms_pages/new
      def new
        @cms_page = CmsPage.new
      end

      # GET /cms_pages/1/edit
      def edit
      end

      # POST /cms_pages
      # POST /cms_pages.json
      def create
          @cms_page = CmsPage.create(cms_page_params)
          if !@cms_page.errors.messages.present?
              redirect_to cms_page_path(@cms_page)
          else
               render :new 
          end
      end

      # PATCH/PUT /cms_pages/1
      # PATCH/PUT /cms_pages/1.json
      def update
          @cms_page.page_name = params[:cms_page][:page_name]
          @cms_page.title = params[:cms_page][:title]
          @cms_page.description = params[:cms_page][:description]
            if @cms_page.save
                  redirect_to @cms_page, notice: 'Cms page was successfully updated.' 
            else
             render :edit 
            end
      end

      # DELETE /cms_pages/1
      # DELETE /cms_pages/1.json
      def destroy
        @cms_page.destroy
        respond_to do |format|
          format.html { redirect_to cms_pages_url, error: 'Cms page was successfully destroyed.' }
          format.json { head :no_content }
        end
      end
 
      private
        # Use callbacks to share common setup or constraints between actions.

        def pass_token
          @user_id = session[:user_id]
          if @user_id.class != NilClass
              CmsPage.current_user_token(session[:user_token])
          else
              redirect_to login_url
          end
        end

        def set_cms_page
              @cms_page = CmsPage.find(params[:id])
        end

        # Never trust parameters from the scary internet, only allow the white list through.
        def cms_page_params
          params.require(:cms_page).permit(:id, :page_name, :title, :description)
        end
end
