class UsersController < ApplicationController
      wrap_parameters :user, include: [:domain_name, :email, :password, :password_confirmation]
      before_action :set_user, only: [:show, :edit, :update, :destroy, :dashboard]
      before_action :admin_user,     only: [:index, :destroy]

      def dashboard
      end

      # GET /users
      # GET /users.json
      def index
          @setting = Setting.first
          @users = User.all
      end
 
      # GET /users/1
      # GET /users/1.json
      def show
           if params[:switch].present?
                 tenant_name = @user.domain_name
                 @tenant_name = tenant_name.gsub! '.', '_'
                 @user = User.find(params[:id],:switch_tenant => @tenant_name)
                 flash[:notice] = 'switched successfully'
           end
      end

      # GET /users/new
      def new
          @user = User.new
      end

      # GET /users/1/edit
      def edit
      end

      # POST /users
      # POST /users.json
      def create
           if session[:user_id]
               redirect_to root_url
          else
              @user = User.create(user_params)
              respond_to do |format|
                if !@user.errors.messages.present?
                  format.html { redirect_to login_path, notice: 'Registration successfull! A message with a confirmation link has been sent to your email address. Please follow the link to activate your account.' }
                  format.json { render :show, warning: :created, location: @user }
                else
                  format.html { render :new, warning: "Registration Unsuccessfull!" }
                  format.json { render json: @user.errors.messages, warning: :unprocessable_entity }
                end
              end
          end
      end

      # PATCH/PUT /users/1
      # PATCH/PUT /users/1.json
      def update
           if params[:user][:domain_name].present?
                @user.domain_name = params[:user][:domain_name]
                if @user.save
                      flash[:notice] = " your domain_name as been successfully updated "
                else
                     flash[:warning] = " domain_name as already been taken by another user "
                end
                redirect_to user_path(@user)
           elsif params[:user][:verified].present?
                @user.verified = params[:user][:verified]
                @user.save
                flash[:notice] = " User has been verified successfully "
                redirect_to users_path
           else
                 if  both_passwords_same? && password_valid?
                      @user.password = params[:user][:password]
                      @user.save
                      flash[:notice] = " Password has been reset. "
                      redirect_to user_path(@user)
                else
                      redirect_to user_path(@user)
                end
          end
      end

      # DELETE /users/1
      # DELETE /users/1.json
      def destroy
           @user.destroy
           redirect_to users_url, notice: 'User was successfully destroyed.' 
      end

      private

          def admin_user
                redirect_to(root_url) unless current_user['admin']
          end

          def both_passwords_blank?
                params[:user][:password].blank? && 
                params[:user][:password_confirmation].blank?
           end

          def both_passwords_same?
            if  params[:user][:password] == params[:user][:password_confirmation]
                return true
             else
                flash[:warning] = " Password confirmation not same. "
                return false
             end
          end

          def password_valid?
                if params[:user][:password].length > 5
                    return true
                else
                    flash[:warning] = " Password should be minimum 6 characters "
                    return false
                end
          end

          # Use callbacks to share common setup or constraints between actions.
          def set_user
                @user_id = session[:user_id]
                if @user_id.class != NilClass
                    User.current_user_token(session[:user_token])
                    if current_user["admin"] && params[:id].present?
                       @user = User.find(params[:id])
                    else
                    @user = User.find(session[:user_id])
                    end
                else
                  redirect_to home_url
                end
          end

          # Never trust parameters from the scary internet, only allow the white list through.
          def user_params
              params.require(:user).permit(:email, :password, :password_confirmation, :domain_name, :verified)
          end
end
