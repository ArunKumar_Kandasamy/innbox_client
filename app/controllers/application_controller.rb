require 'rubygems'
require 'active_resource'  
class ApplicationController < ActionController::Base
      # Prevent CSRF attacks by raising an exception.
      # For APIs, you may want to use :null_session instead.

      protect_from_forgery 
       rescue_from ActiveResource::UnauthorizedAccess do
             if current_user 
                   redirect_to dashboard_path
             else
                flash[:notice] = 'The page you tried to access does not exist'
                redirect_to login_path
            end
      end

   private

      def current_user
            if session[:user_id]
                 User.current_user_token(session[:user_token])
                 @current_user ||= User.get(session[:user_id])
            end
      end
      helper_method :current_user

      def authorize
        redirect_to login_url, alert: "Not authorized" if current_user.nil?
      end
end

