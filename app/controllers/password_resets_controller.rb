class PasswordResetsController < ApplicationController

      def new
      end

      def create
          @user = PasswordReset.create(email: params[:password_reset][:email].downcase)
          if !@user.errors.messages.present?
            flash[:notice] = "Email sent with password reset instructions"
            redirect_to login_url
          else
            flash.now[:warning] = "Email address not found"
            render 'new'
          end
      end
      

      def edit
          @user_attributes = PasswordReset.create(id: params[:id], email: params[:email].downcase, token: 1)
          @user = @user_attributes.attributes
          render 'edit'
      end
  
      def update
            if both_passwords_blank?
                  flash.now[:danger] = "Password/confirmation can't be blank"
                  render 'edit'
            else 
                   @user = PasswordReset.create(:password => params[:user][:password],:email => params[:email],:uid => params[:uid])
                   if @user 
                          flash[:notice] = "Password has been reset."
                          redirect_to login_url
                    else
                          render 'edit'
                    end
            end
      end
      
      private
        
        def user_params
          params.require(:user).permit(:password, :password_confirmation,:email)
        end
        
        # Returns true if password & confirmation are blank.
        def both_passwords_blank?
          params[:user][:password].blank? && 
          params[:user][:password_confirmation].blank?
        end

        def both_passwords_same?
             return params[:user][:password] == params[:user][:password_confirmation]  ? true :  false
        end

        def password_valid?
             return params[:user][:password].lenght > 5  ? true :  false
        end
      
        # Before filters
      
        def get_user
          User.current_user_token(session[:user_token])
          @user = User.find(params[:email])
        end
        
        # Confirms a valid user.
        def valid_user
          unless (@user && @user.activated? && 
                  @user.authenticated?(:reset, params[:id]))
            redirect_to root_url
          end      
        end
        
        # Checks expiration of reset token.
        def check_expiration
          if @user.password_reset_expired?
            flash[:danger] = "Password reset has expired."
            redirect_to new_password_reset_url
          end
        end
end
