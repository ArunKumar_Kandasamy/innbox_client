Rails.application.routes.draw do
  


      mount Ckeditor::Engine => '/ckeditor'
      resources :gallery_types do
              resources :gallery_images do
                  collection { post :sort }
              end
       end
      resources :settings
      resources :cms_pages
      resources :users
      resources :sessions
      post   'login'   => 'sessions#create'
      get 'login', to: 'sessions#new', as: 'log_in'
      get 'logout', to: 'sessions#destroy', as: 'logout'
      resources :account_activations, only: [:edit]
      resources :password_resets,     only: [:new, :create, :edit, :show]
      post   'password_resets/:id'   => 'password_resets#update'
      get '/', to: 'users#dashboard', :as => "dashboard"
      get 'policy' => 'static_pages#policy', :as => :policy
      get 'terms' => 'static_pages#terms', :as => :terms
      get 'documentation' => 'static_pages#documentation', :as => :documentation
      get 'home' => 'static_pages#index', :as => :home
      root 'users#dashboard'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
